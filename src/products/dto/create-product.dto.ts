import { IsNotEmpty, Length } from 'class-validator';

export class CreateProductDto {
  @IsNotEmpty()
  @Length(5)
  name: string;

  @IsNotEmpty()
  price: number;
}
