import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 16, name: 'user_login', unique: true })
  login: string;

  @Column({ name: 'user_name' })
  name: string;

  @Column({ name: 'user_password' })
  password: string;
}
